﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Neuron;

public class TestTransform
{
    public Vector3 localPosition;
    public Quaternion localRotation;
    public Vector3 vectorRotation
    {
        get
        {
            return localRotation.eulerAngles;
        }
    }

    public TestTransform(Vector3 Position, Quaternion Rotation)
    {
        this.localPosition = Position;
        this.localRotation = Rotation;
    }

    public override string ToString()
    {
        return string.Format(
            "Position: {0}, Rotation: {1}, EulerRotation: {2}",
            localPosition, localRotation, vectorRotation
        );
    }

    public static TestTransform operator +(TestTransform a, TestTransform b)
    {
        return new TestTransform(
            a.localPosition + b.localPosition,
            a.localRotation * b.localRotation
        );
    }

    public static TestTransform operator -(TestTransform a, TestTransform b)
    {
        return new TestTransform(
            a.localPosition - b.localPosition,
            a.localRotation * Quaternion.Inverse(b.localRotation)
        );
    }
}

public class FreeTransform
{
    #region Constructors
    public FreeTransform()
    {
        bone = NeuronBones.NumOfBones;
        parent = null;
        localPosition = Vector3.zero;
        localRotation = Quaternion.identity;
        children = new List<FreeTransform>();
    }

    public FreeTransform(NeuronBones bone)
    {
        this.bone = bone;
        parent = null;
        localPosition = Vector3.zero;
        localRotation = Quaternion.identity;
        vectorRotation = Vector3.zero;
        children = new List<FreeTransform>();
    }
    #endregion

    public NeuronBones bone;
    public FreeTransform parent;
    public Vector3 localPosition;
    public Quaternion localRotation;
    public Vector3 vectorRotation;
    public List<FreeTransform> children;

    #region Generation methods
    public void addChild(FreeTransform child)
    {
        if (child.parent != null)
        {
            child.parent.removeChild(child);
        }
        child.parent = this;
        children.Add(child);
    }

    public void removeChild(FreeTransform child)
    {
        children.Remove(child);
    }

    public void setParent(FreeTransform parent)
    {
        parent.removeChild(this);
        removeChild(parent);
        this.parent = parent;
    }

    public void clearGeneration()
    {
        parent = null;
        foreach (FreeTransform e in children)
        {
            children.Remove(e);
        }
    }
    #endregion

    public void addPosition(Vector3 pos)
    {
        localPosition += pos;
    }

    public void addPositionChildrenIntact(Vector3 pos)
    {
        addPosition(pos);
        foreach (FreeTransform e in children)
        {
            addPosition(-pos);
        }
    }

    public void addRotation(Quaternion rot)
    {
        localRotation *= rot;
    }

    public void addRotationInverse(Quaternion rot)
    {
        localRotation *= Quaternion.Inverse(rot);
    }

    public void rotatePosition(Quaternion rot)
    {
        localPosition = rot * localPosition;
    }

    public void rotatePositionInverse(Quaternion rot)
    {
        localPosition = Quaternion.Inverse(rot) * localPosition;
    }

    public void addRotationChildrenIntact(Quaternion rot)
    {
        addRotation(rot);
        foreach (FreeTransform e in children)
        {
            addRotationInverse(rot);
            rotatePositionInverse(rot);
        }
    }

    public static FreeTransform operator +(FreeTransform a, FreeTransform b)
    {
        FreeTransform result = new FreeTransform(a.bone);
        result.localPosition = a.localPosition;
        result.localRotation = a.localRotation;
        result.localPosition += b.localPosition;
        result.localRotation *= b.localRotation;
        result.vectorRotation = result.localRotation.eulerAngles;
        return result;
    }

    public static FreeTransform operator +(FreeTransform a, Transform b)
    {
        FreeTransform result = new FreeTransform(a.bone);
        result.localPosition = a.localPosition;
        result.localRotation = a.localRotation;
        result.localPosition += b.localPosition;
        result.localRotation *= b.localRotation;
        result.vectorRotation = result.localRotation.eulerAngles;
        return result;
    }

    public static FreeTransform operator -(FreeTransform a, FreeTransform b)
    {
        FreeTransform result = new FreeTransform(a.bone);
        result.localPosition = a.localPosition;
        result.localRotation = a.localRotation;
        result.localPosition -= b.localPosition;
        result.localRotation *= Quaternion.Inverse(b.localRotation);
        result.vectorRotation = result.localRotation.eulerAngles;
        return result;
    }

    public static FreeTransform operator -(FreeTransform a, Transform b)
    {
        FreeTransform result = new FreeTransform(a.bone);
        result.localPosition = a.localPosition;
        result.localRotation = a.localRotation;
        result.localPosition -= b.localPosition;
        result.localRotation *= Quaternion.Inverse(b.localRotation);
        result.vectorRotation = result.localRotation.eulerAngles;
        return result;
    }
}
