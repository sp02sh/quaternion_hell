﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Neuron;
using System;

public class NeuronOffsetMaker : MonoBehaviour {

    public Vector3[] posePositions = new Vector3[(int)NeuronBones.NumOfBones];
    public Vector3[] poseRotations = new Vector3[(int)NeuronBones.NumOfBones];
    public Vector3[] bonePositions = new Vector3[(int)NeuronBones.NumOfBones];
    public Vector3[] boneRotations = new Vector3[(int)NeuronBones.NumOfBones];
    public NeuronTransformsInstance source;
    public NeuronTransformsInstance player;

	// Use this for initialization
	void Start () {
        posePositions = new Vector3[(int)NeuronBones.NumOfBones];
        poseRotations = new Vector3[(int)NeuronBones.NumOfBones];
        bonePositions = new Vector3[(int)NeuronBones.NumOfBones];
        boneRotations = new Vector3[(int)NeuronBones.NumOfBones];
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void generateOffsets()
    {
        NeuronTopology inputTopology = new NeuronTopology();
        // initial output topology has built in offsets to transforms
        NeuronTopology initialOutputTopology = new NeuronTopology();
        NeuronTopology offsetOutputTopology = new NeuronTopology();
        NeuronTopology clearOutputTopology = new NeuronTopology();
        NeuronTopology differenceTopology = new NeuronTopology();
        NeuronTopology resultTopology = new NeuronTopology();
        int exampleBone = 1;
        inputTopology.setTransforms(bonePositions, boneRotations);
        initialOutputTopology = extractTopologyFromPlayer(player);
        offsetOutputTopology = extractOffsetsFromPlayer(player);
        clearOutputTopology = initialOutputTopology - offsetOutputTopology;
        differenceTopology = inputTopology - clearOutputTopology;
        resultTopology = clearOutputTopology + differenceTopology;
        Debug.Log("input topology = " + inputTopology.vectorRotations[exampleBone]);
        Debug.Log("initial output topology = " + initialOutputTopology.vectorRotations[exampleBone]);
        Debug.Log("offset output topology = " + offsetOutputTopology.vectorRotations[exampleBone]);
        Debug.Log("clear output topology = " + clearOutputTopology.vectorRotations[exampleBone]);
        Debug.Log("difference topology = " + differenceTopology.vectorRotations[exampleBone]);
        Debug.Log("result topology = " + resultTopology.vectorRotations[exampleBone]);
        Debug.Log("Full rotation: " + GeometryMath.fullRotation);
        posePositions = resultTopology.positions;
        poseRotations = resultTopology.vectorRotations;
    }

    private Quaternion[] asQuaternion(Vector3[] boneRotations)
    {
        Quaternion[] result = new Quaternion[boneRotations.Length];
        for (int i = 0; i < result.Length; i++)
        {
            result[i].eulerAngles = boneRotations[i];
        }
        return result;
    }

    private Vector3[] asVector3(Quaternion[] boneRotations)
    {
        Vector3[] result = new Vector3[boneRotations.Length];
        for (int i = 0; i < result.Length; i++)
        {
            result[i] = boneRotations[i].eulerAngles;
        }
        return result;
    }

    private NeuronTopology extractTopologyFromPlayer(NeuronTransformsInstance player)
    {
        NeuronTopology result = new NeuronTopology();
        result.positions = extractPositionsFromPlayer(player);
        result.rotations = extractRotationsFromPlayer(player);
        return result;
    }

    private NeuronTopology extractOffsetsFromPlayer(NeuronTransformsInstance player)
    {
        NeuronTopology result = new NeuronTopology();
        result.positions = player.bonePositionOffsets;
        result.vectorRotations = player.boneRotationOffsets;
        return result;
    }

    private Vector3[] extractPositionsFromPlayer(NeuronTransformsInstance player)
    {
        Transform[] playerTransforms = player.transforms;
        Vector3[] result = new Vector3[playerTransforms.Length];
        for (int i = 0; i < result.Length; i++)
        {
            result[i] = playerTransforms[i].localPosition;
        }
        return result;
    }

    private Quaternion[] extractRotationsFromPlayer(NeuronTransformsInstance player)
    {
        Transform[] playerTransforms = player.transforms;
        Quaternion[] result = new Quaternion[playerTransforms.Length];
        for (int i = 0; i < result.Length; i++)
        {
            result[i] = playerTransforms[i].localRotation;
        }
        return result;
    }

    //private Vector3[] extractRotationsFromPlayer(NeuronTransformsInstance player)
    //{
    //    Transform[] playerTransforms = player.transforms;
    //    Vector3[] result = new Vector3[playerTransforms.Length];
    //    for (int i = 0; i < result.Length; i++)
    //    {
    //        result[i] = playerTransforms[i].localRotation.eulerAngles;
    //    }
    //    return result;
    //}

    private Vector3[] diffTopologyPositions(NeuronTopology topologyA, NeuronTopology topologyB)
    {
        Vector3[] positionsA = topologyA.positions;
        Vector3[] positionsB = topologyB.positions;
        Vector3[] result = new Vector3[positionsA.Length];
        for (int i = 0; i < result.Length; i++)
        {
            result[i] = positionsA[i] - positionsB[i];
        }
        return result;
    }

    private Quaternion[] diffTopologyRotations(NeuronTopology topologyA, NeuronTopology topologyB)
    {
        Quaternion[] rotationsA = topologyA.rotations;
        Quaternion[] rotationsB = topologyB.rotations;
        Quaternion[] result = new Quaternion[rotationsA.Length];
        for (int i = 0; i < result.Length; i++)
        {
            result[i] = rotationsA[i] * Quaternion.Inverse(rotationsB[i]);
        }
        return result;
    }

    public void applyOffsets()
    {
        if (player != null)
        {
            for (int i = 0; i < (int)NeuronBones.NumOfBones; i++)
            {
                player.bonePositionOffsets[i] = posePositions[i];
                player.boneRotationOffsets[i] = poseRotations[i];
            }
        }
        else
        {
            Debug.LogError("Player is not set!");
        }
    }
}
