﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    void Start()
    {
        // test 1
        TestTransform t0 = new TestTransform(
            Vector3.zero,
            Quaternion.identity
        );
        Debug.Log("t0: " + t0);

        TestTransform t1 = new TestTransform(
            new Vector3(1.0f, 0.0f, 0.0f),
            Quaternion.Euler(90.0f, 0.0f, 0.0f)
        );
        Debug.Log("t1: " + t1);

        TestTransform sum0 = t0 + t1;
        Debug.Log("sum0: " + sum0);

        Debug.Assert(
            sum0.localPosition == new Vector3(1.0f, 0.0f, 0.0f)
            && sum0.vectorRotation == new Vector3(90.0f, 0.0f, 0.0f)
        );

        // test 2
        TestTransform t3 = new TestTransform(
            new Vector3(1.0f, 0.0f, 0.0f),
            Quaternion.Euler(90.0f, 45.0f, 0.0f)
        );
        Debug.Log("t3: " + t3);

        TestTransform t4 = new TestTransform(
            new Vector3(1.0f, 0.0f, 0.0f),
            Quaternion.Euler(60.0f, -45.0f, 30.0f) // in fact 60.0, 315.0, 30.0\
        );
        Debug.Log("t4: " + t4);

        TestTransform sum1 = t3 + t4;
        Debug.Log("sum1: " + sum1);

        TestTransform t5 = sum1 - t4;
        Debug.Log("t5: " + t5);

        Debug.Assert(
            t5.localPosition == t3.localPosition
            && t5.localRotation == t3.localRotation
        );
    }
}
