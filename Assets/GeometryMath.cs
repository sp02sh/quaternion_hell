﻿using UnityEngine;
using System.Collections;

public class GeometryMath {

    public static readonly Vector3 fullRotation = new Vector3(360f, 360f, 360f);

	public static Vector3 RotatePoint(Vector3 point, Quaternion rotation)
    {
        return rotation * point;
    }

    public static Vector3 InverseRotatePoint(Vector3 point, Quaternion rotation)
    {
        return RotatePoint(point, Quaternion.Euler(-rotation.eulerAngles));
    }

    public static Vector3 Modulo(float mod, Vector3 v)
    {
        return new Vector3(Mathf.Repeat(mod, v.x), Mathf.Repeat(mod, v.y), Mathf.Repeat(mod, v.z));
    }
}
