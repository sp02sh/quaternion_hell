﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Neuron;

public class NeuronTopology
{
    public FreeTransform[] transforms;
    public Vector3[] positions
    {
        get { return getPositions(); }
        set { setPositions(value); }
    }
    public Quaternion[] rotations
    {
        get { return getRotations(); }
        set { setRotations(value); }
    }
    public Vector3[] vectorRotations
    {
        get { return getVectorRotations(); }
        set { setVectorRotations(value); }
    }

    public NeuronTopology()
    {
        int numOfBones = (int)NeuronBones.NumOfBones;
        transforms = new FreeTransform[numOfBones];
        for (int i = 0; i < numOfBones; i++)
        {
            transforms[i] = new FreeTransform((NeuronBones)i);
        }
        setDefaultTopology();
    }

    public void setDefaultTopology()
    {
        foreach(FreeTransform e in transforms)
        {
            e.clearGeneration();
        }
        #region Hips
        transforms[(int)NeuronBones.Hips].addChild(transforms[(int)NeuronBones.LeftUpLeg]);
        #region LeftUpLeg
        transforms[(int)NeuronBones.LeftUpLeg].addChild(transforms[(int)NeuronBones.LeftLeg]);
        #region LeftLeg
        transforms[(int)NeuronBones.LeftLeg].addChild(transforms[(int)NeuronBones.LeftFoot]);
        #endregion
        #endregion
        transforms[(int)NeuronBones.Hips].addChild(transforms[(int)NeuronBones.RightUpLeg]);
        #region RightUpLeg
        transforms[(int)NeuronBones.RightUpLeg].addChild(transforms[(int)NeuronBones.RightLeg]);
        #region RightLeg
        transforms[(int)NeuronBones.RightLeg].addChild(transforms[(int)NeuronBones.RightFoot]);
        #endregion
        #endregion
        transforms[(int)NeuronBones.Hips].addChild(transforms[(int)NeuronBones.Spine]);
        #region Spine
        transforms[(int)NeuronBones.Spine].addChild(transforms[(int)NeuronBones.Spine1]);
        #region Spine1
        transforms[(int)NeuronBones.Spine1].addChild(transforms[(int)NeuronBones.Spine2]);
        #region Spine2
        transforms[(int)NeuronBones.Spine2].addChild(transforms[(int)NeuronBones.Spine3]);
        #region Spine3
        transforms[(int)NeuronBones.Spine3].addChild(transforms[(int)NeuronBones.LeftShoulder]);
        #region LeftShoulder
        transforms[(int)NeuronBones.LeftShoulder].addChild(transforms[(int)NeuronBones.LeftArm]);
        #region LeftArm
        transforms[(int)NeuronBones.LeftArm].addChild(transforms[(int)NeuronBones.LeftForeArm]);
        #region LeftForeArm
        transforms[(int)NeuronBones.LeftForeArm].addChild(transforms[(int)NeuronBones.LeftHand]);
        #region LeftHand
        transforms[(int)NeuronBones.LeftHand].addChild(transforms[(int)NeuronBones.LeftHandThumb1]);
        #region LeftHandThumb1
        transforms[(int)NeuronBones.LeftHandThumb1].addChild(transforms[(int)NeuronBones.LeftHandThumb2]);
        #region LeftHandThumb2
        transforms[(int)NeuronBones.LeftHandThumb2].addChild(transforms[(int)NeuronBones.LeftHandThumb3]);
        #endregion
        #endregion
        transforms[(int)NeuronBones.LeftHand].addChild(transforms[(int)NeuronBones.LeftInHandIndex]);
        #region LeftInHandIndex
        transforms[(int)NeuronBones.LeftInHandIndex].addChild(transforms[(int)NeuronBones.LeftHandIndex1]);
        #region LeftHandIndex1
        transforms[(int)NeuronBones.LeftHandIndex1].addChild(transforms[(int)NeuronBones.LeftHandIndex2]);
        #region LeftHandIndex2
        transforms[(int)NeuronBones.LeftHandIndex2].addChild(transforms[(int)NeuronBones.LeftHandIndex3]);
        #endregion
        #endregion
        #endregion
        transforms[(int)NeuronBones.LeftHand].addChild(transforms[(int)NeuronBones.LeftInHandMiddle]);
        #region LeftInHandMiddle
        transforms[(int)NeuronBones.LeftInHandMiddle].addChild(transforms[(int)NeuronBones.LeftHandMiddle1]);
        #region LeftHandMiddle1
        transforms[(int)NeuronBones.LeftHandMiddle1].addChild(transforms[(int)NeuronBones.LeftHandMiddle2]);
        #region LeftHandMiddle2
        transforms[(int)NeuronBones.LeftHandMiddle2].addChild(transforms[(int)NeuronBones.LeftHandMiddle3]);
        #endregion
        #endregion
        #endregion
        transforms[(int)NeuronBones.LeftHand].addChild(transforms[(int)NeuronBones.LeftInHandPinky]);
        #region LeftInHandPinky
        transforms[(int)NeuronBones.LeftInHandPinky].addChild(transforms[(int)NeuronBones.LeftHandPinky1]);
        #region LeftHandPinky1
        transforms[(int)NeuronBones.LeftHandPinky1].addChild(transforms[(int)NeuronBones.LeftHandPinky2]);
        #region LeftHandPinky2
        transforms[(int)NeuronBones.LeftHandPinky2].addChild(transforms[(int)NeuronBones.LeftHandPinky3]);
        #endregion
        #endregion
        #endregion
        transforms[(int)NeuronBones.LeftHand].addChild(transforms[(int)NeuronBones.LeftInHandRing]);
        #region LeftInHandRing
        transforms[(int)NeuronBones.LeftInHandRing].addChild(transforms[(int)NeuronBones.LeftHandRing1]);
        #region LeftHandRing1
        transforms[(int)NeuronBones.LeftHandRing1].addChild(transforms[(int)NeuronBones.LeftHandRing2]);
        #region LeftHandRing2
        transforms[(int)NeuronBones.LeftHandRing2].addChild(transforms[(int)NeuronBones.LeftHandRing3]);
        #endregion
        #endregion
        #endregion
        #endregion
        #endregion
        #endregion
        #endregion
        transforms[(int)NeuronBones.Spine3].addChild(transforms[(int)NeuronBones.Neck]);
        #region Neck
        transforms[(int)NeuronBones.Neck].addChild(transforms[(int)NeuronBones.Head]);
        #endregion
        transforms[(int)NeuronBones.Spine3].addChild(transforms[(int)NeuronBones.RightShoulder]);
        #region RightShoulder
        transforms[(int)NeuronBones.RightShoulder].addChild(transforms[(int)NeuronBones.RightArm]);
        #region RightArm
        transforms[(int)NeuronBones.RightArm].addChild(transforms[(int)NeuronBones.RightForeArm]);
        #region RightForeArm
        transforms[(int)NeuronBones.RightForeArm].addChild(transforms[(int)NeuronBones.RightHand]);
        #region RightHand
        transforms[(int)NeuronBones.RightHand].addChild(transforms[(int)NeuronBones.RightHandThumb1]);
        #region RightHandThumb1
        transforms[(int)NeuronBones.RightHandThumb1].addChild(transforms[(int)NeuronBones.RightHandThumb2]);
        #region RightHandThumb2
        transforms[(int)NeuronBones.RightHandThumb2].addChild(transforms[(int)NeuronBones.RightHandThumb3]);
        #endregion
        #endregion
        transforms[(int)NeuronBones.RightHand].addChild(transforms[(int)NeuronBones.RightInHandIndex]);
        #region RightInHandIndex
        transforms[(int)NeuronBones.RightInHandIndex].addChild(transforms[(int)NeuronBones.RightHandIndex1]);
        #region RightHandIndex1
        transforms[(int)NeuronBones.RightHandIndex1].addChild(transforms[(int)NeuronBones.RightHandIndex2]);
        #region RightHandIndex2
        transforms[(int)NeuronBones.RightHandIndex2].addChild(transforms[(int)NeuronBones.RightHandIndex3]);
        #endregion
        #endregion
        #endregion
        transforms[(int)NeuronBones.RightHand].addChild(transforms[(int)NeuronBones.RightInHandMiddle]);
        #region RightInHandMiddle
        transforms[(int)NeuronBones.RightInHandMiddle].addChild(transforms[(int)NeuronBones.RightHandMiddle1]);
        #region RightHandMiddle1
        transforms[(int)NeuronBones.RightHandMiddle1].addChild(transforms[(int)NeuronBones.RightHandMiddle2]);
        #region RightHandMiddle2
        transforms[(int)NeuronBones.RightHandMiddle2].addChild(transforms[(int)NeuronBones.RightHandMiddle3]);
        #endregion
        #endregion
        #endregion
        transforms[(int)NeuronBones.RightHand].addChild(transforms[(int)NeuronBones.RightInHandPinky]);
        #region RightInHandPinky
        transforms[(int)NeuronBones.RightInHandPinky].addChild(transforms[(int)NeuronBones.RightHandPinky1]);
        #region RightHandPinky1
        transforms[(int)NeuronBones.RightHandPinky1].addChild(transforms[(int)NeuronBones.RightHandPinky2]);
        #region RightHandPinky2
        transforms[(int)NeuronBones.RightHandPinky2].addChild(transforms[(int)NeuronBones.RightHandPinky3]);
        #endregion
        #endregion
        #endregion
        transforms[(int)NeuronBones.RightHand].addChild(transforms[(int)NeuronBones.RightInHandRing]);
        #region RightInHandRing
        transforms[(int)NeuronBones.RightInHandRing].addChild(transforms[(int)NeuronBones.RightHandRing1]);
        #region RightHandRing1
        transforms[(int)NeuronBones.RightHandRing1].addChild(transforms[(int)NeuronBones.RightHandRing2]);
        #region RightHandRing2
        transforms[(int)NeuronBones.RightHandRing2].addChild(transforms[(int)NeuronBones.RightHandRing3]);
        #endregion
        #endregion
        #endregion
        #endregion
        #endregion
        #endregion
        #endregion
        #endregion
        #endregion
        #endregion
        #endregion
        #endregion
    }

    #region Single FreeTransform processing
    public void addPosition(Vector3 pos, NeuronBones bone)
    {
        transforms[(int)bone].addPosition(pos);
    }

    public void addPositionChildrenIntact(Vector3 pos, NeuronBones bone)
    {
        transforms[(int)bone].addPositionChildrenIntact(pos);
    }

    public void addRotation(Quaternion rot, NeuronBones bone)
    {
        transforms[(int)bone].addRotation(rot);
    }

    public void addRotationChildrenIntact(Quaternion rot, NeuronBones bone)
    {
        transforms[(int)bone].addRotationChildrenIntact(rot);
    }
    #endregion

    #region Multiple FreeTransform processing
    public void addPositions(Vector3[] pos)
    {
        for (int i = 0; i < (int)NeuronBones.NumOfBones; i++)
        {
            addPosition(pos[i], (NeuronBones)i);
        }
    }

    public void addPositionsChildrenIntact(Vector3[] pos)
    {
        for (int i = 0; i < (int)NeuronBones.NumOfBones; i++)
        {
            addPositionChildrenIntact(pos[i], (NeuronBones)i);
        }
    }

    public void addPositionsLocal(Transform[] trans)
    {
        Vector3[] pos = new Vector3[(int)NeuronBones.NumOfBones];
        for (int i = 0; i < (int)NeuronBones.NumOfBones; i++)
        {
            pos[i] = trans[i].localPosition;
        }
        addPositions(pos);
    }

    public void substractPositionsLocal(Transform[] trans)
    {
        Vector3[] pos = new Vector3[(int)NeuronBones.NumOfBones];
        for (int i = 0; i < (int)NeuronBones.NumOfBones; i++)
        {
            pos[i] = trans[i].localPosition;
        }
        substractPositions(pos);
    }

    public void substractPositions(Vector3[] pos)
    {
        for (int i = 0; i < (int)NeuronBones.NumOfBones; i++)
        {
            transforms[i].addPosition(-pos[i]);
        }
    }

    public void substractPositionsChildrenIntact(Vector3[] pos)
    {
        for (int i = 0; i < (int)NeuronBones.NumOfBones; i++)
        {
            addPositionChildrenIntact(-pos[i], (NeuronBones)i);
        }
    }

    public void addRotations(Quaternion[] rot)
    {
        for (int i = 0; i < (int)NeuronBones.NumOfBones; i++)
        {
            addRotation(rot[i], (NeuronBones)i);
        }
    }

    public void addRotations(Vector3[] rot)
    {
        for (int i = 0; i < (int)NeuronBones.NumOfBones; i++)
        {
            addRotation(Quaternion.Euler(rot[i]), (NeuronBones)i);
        }
    }

    public void substractRotations(Vector3[] rot)
    {
        for (int i = 0; i < (int)NeuronBones.NumOfBones; i++)
        {
            addRotation(Quaternion.Inverse(Quaternion.Euler(rot[i])), (NeuronBones)i);
        }
    }

    public void substractVectorRotations(Vector3[] rot)
    {
        for (int i = 0; i < (int)NeuronBones.NumOfBones; i++)
        {
            addVectorRotation(-rot[i], (NeuronBones)i);
        }
    }

    public void addVectorRotation(Vector3 rot, NeuronBones bone)
    {
        //vectorRotations[(int)bone] += GeometryMath.Modulo(360f, vectorRotations[(int)bone] + rot + GeometryMath.fullRotation);
        vectorRotations[(int)bone] += rot;
    }

    public void addRotationsLocal(Transform[] trans)
    {
        Quaternion[] rot = new Quaternion[(int)NeuronBones.NumOfBones];
        for (int i = 0; i < (int)NeuronBones.NumOfBones; i++)
        {
            rot[i] = trans[i].localRotation;
        }
        addRotations(rot);
    }

    public void substractRotationsLocal(Transform[] trans)
    {
        Quaternion[] rot = new Quaternion[(int)NeuronBones.NumOfBones];
        for (int i = 0; i < (int)NeuronBones.NumOfBones; i++)
        {
            rot[i] = trans[i].localRotation;
        }
        substractRotations(rot);
    }

    public void addRotationsChildrenIntact(Quaternion[] rot)
    {
        for (int i = 0; i < (int)NeuronBones.NumOfBones; i++)
        {
            addRotationChildrenIntact(rot[i], (NeuronBones)i);
        }
    }

    public void substractRotations(Quaternion[] rot)
    {
        for (int i = 0; i < (int)NeuronBones.NumOfBones; i++)
        {
            addRotation(Quaternion.Inverse(rot[i]), (NeuronBones)i);
        }
    }

    public void substractRotationsChildrenIntact(Quaternion[] rot)
    {
        for (int i = 0; i < (int)NeuronBones.NumOfBones; i++)
        {
            addRotationChildrenIntact(Quaternion.Inverse(rot[i]), (NeuronBones)i);
        }
    }

    public void addTransformsLocal(Transform[] transforms)
    {
        addPositionsLocal(transforms);
        addRotationsLocal(transforms);
    }

    public void substractTransformsLocal(Transform[] transforms)
    {
        substractPositionsLocal(transforms);
        substractRotationsLocal(transforms);
    }
    #endregion

    #region FreeTransform Data Input/Output
    public Vector3[] getPositions()
    {
        Vector3[] result = new Vector3[transforms.Length];
        for (int i = 0; i < result.Length; i++)
        {
            result[i] = transforms[i].localPosition;
        }
        return result;
    }

    public void setPositions(Vector3[] positions)
    {
        for (int i = 0; i < positions.Length; i++)
        {
            transforms[i].localPosition = positions[i];
        }
    }

    public Quaternion[] getRotations()
    {
        Quaternion[] result = new Quaternion[transforms.Length];
        for (int i = 0; i < result.Length; i++)
        {
            result[i] = transforms[i].localRotation;
        }
        return result;
    }

    public Vector3[] getVectorRotations()
    {
        Vector3[] result = new Vector3[transforms.Length];
        for (int i = 0; i < result.Length; i++)
        {
            result[i] = transforms[i].vectorRotation;
        }
        return result;
    }

    public void setRotations(Quaternion[] rotations)
    {
        for (int i = 0; i < positions.Length; i++)
        {
            transforms[i].localRotation = rotations[i];
            transforms[i].vectorRotation = rotations[i].eulerAngles;
        }
    }

    public void setVectorRotations(Vector3[] rotations)
    {
        for (int i = 0; i < positions.Length; i++)
        {
            transforms[i].vectorRotation = rotations[i];
            transforms[i].localRotation = Quaternion.Euler(rotations[i]);
        }
    }

    public void setTransforms(Vector3[] positions, Quaternion[] rotations)
    {
        this.positions = positions;
        this.rotations = rotations;
    }

    public void setTransforms(Vector3[] positions, Vector3[] rotations)
    {
        this.positions = positions;
        this.vectorRotations = rotations;
    }
    #endregion

    #region Operators
    public static NeuronTopology operator +(NeuronTopology a, NeuronTopology b)
    {
        NeuronTopology result = new NeuronTopology();
        for (int i = 0; i < (int)NeuronBones.NumOfBones; i++)
        {
            result.transforms[i] = a.transforms[i] + b.transforms[i];
        }
        //result.positions = a.positions;
        //result.rotations = a.rotations;
        //result.addPositions(b.positions);
        //result.addRotations(b.rotations);
        return result;
    }

    public static NeuronTopology operator +(NeuronTopology a, Transform[] b)
    {
        NeuronTopology result = new NeuronTopology();
        for (int i = 0; i < (int)NeuronBones.NumOfBones; i++)
        {
            result.transforms[i] = a.transforms[i] + b[i];
        }
        //result.positions = a.positions;
        //result.rotations = a.rotations;
        //result.addTransformsLocal(b);
        return result;
    }

    public static NeuronTopology operator -(NeuronTopology a, NeuronTopology b)
    {
        NeuronTopology result = new NeuronTopology();
        for(int i=0;i<(int)NeuronBones.NumOfBones;i++)
        {
            result.transforms[i] = a.transforms[i] - b.transforms[i];
        }
        //result.positions = a.positions;
        //result.vectorRotations = a.vectorRotations;
        //result.substractPositions(b.positions);
        //result.substractVectorRotations(b.vectorRotations);
        return result;
    }

    public static NeuronTopology operator -(NeuronTopology a, Transform[] b)
    {
        NeuronTopology result = new NeuronTopology();
        for (int i = 0; i < (int)NeuronBones.NumOfBones; i++)
        {
            result.transforms[i] = a.transforms[i] - b[i];
        }
        //result.positions = a.positions;
        //result.rotations = a.rotations;
        //result.substractTransformsLocal(b);
        return result;
    }
    #endregion
}
